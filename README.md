Overview
========================

Spire.PDFViewer is a professional and powerful .NET PDF Viewer component, which offers several controls for developer to create WinForm applications to open, view and print PDF document. 

Functions
===========================

Open and View

Spire.PDFViewer for .NET can open and view PDF/A-1B, PDF/X1A and even encrypted document.

When viewing, developers can view with zoom in/out, page up/down, moving to the first/last page or fitting page/width. Also, hand tool is available.

Elements

Spire.PDFViewer for .NET supports kinds of PDF fonts, including TrueType, Type0, Type1, Type3, OpenType and CJK font. It also support hyperlinks, DeviceN color space and JPEG2000, DCT, CCITT Fax Image format in PDF.

Print

Spire.PDFViewer enables developers to print PDF document and supports page setup before printing, such as margins, page orientation, page size etc. Also, developers can choose specified one page, current page, mulitple pages or all pages to print.

PDF to Image

Spire.PDFViewer for .NET enables developers to save PDF pages(specified one, current page, mulitple pages, all pages) to image formats(.bmp, .png, .jpeg). 

========
Learn more about Spire.PDFViewer for .NET: http://www.e-iceblue.com/Introduce/pdf-viewer.html

Free Trial: http://www.e-iceblue.com/Download/download-pdf-viewer-for-net-now.html
